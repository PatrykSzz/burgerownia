<?php
session_start();
if (!isset($_SESSION['zalogowany']))
	{
		header('Location: cart.php');
		exit();
	}				

?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Royal Burgers</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link href="style.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<header>
				<div class="logo"><img src="images/logo.png"></div>
				<div class="menu">
					<a href="index_zalogowany.php">Home</a>
					<a href="menu_zalogowany.php">Menu</a>
					<a href="cart_zalogowany.php" class="active">Koszyk</a>
					<a href="wyloguj.php">Wyloguj <?php echo $_SESSION['email']; ?></a> 
				</div>
			</header>
			<div class="cart_content">
				<div class="order">
<?php
if(!isset($_SESSION['cart'])) {
		echo "Koszyk jest pusty";
	} else {
		// Pobieranie produktów z bazy
		// Łączenie z bazą
		$db = mysqli_connect("localhost", "root", "", "burgerownia");
		// Przygotowanie polecenia
		$stmt = $db->prepare("SELECT * FROM produkty WHERE id IN (".implode(',',$_SESSION['cart']).")");
		// Wykonanie polecenia
		$stmt->execute();
		// Fetch all products
		$products = $stmt->get_result();
	
		// Obliczanie ceny
		$total_price = 0;
		foreach($products as $product) {
			$total_price += $product['price'];
		}
	
  echo "<table>";
  echo "<tr><th>Nazwa produktu</th><th>Cena</th></tr>";
  foreach($products as $product) {
	  echo "<tr>";
	  echo "<th>".$product['name']."</th>";
	  echo "<th>".$product['price']."</th>";
	  echo "</tr>";
  }
  echo "<tr><th>Razem:</th><th>".$total_price."</th></tr>";
  echo "</table>";
	}
?><a href="zamawiam.php"> Zamawaim</a>
				</div>
			</div>
			<footer>
				<div>ul. Przemysłowa 20,<br />61-872 Poznań</div>
				<div><b>Godziny otwarcia:</b><br />Pn-Nd 11:00-23:00</div>
				<div><b>Telefon:</b> 800 758 749<br /><b>E-mail:</b> rburgers@gmail.com
			</footer>
		</div>
	</body>
</html>