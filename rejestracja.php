<?php
 
 if (isset($_REQUEST['action']) && $_REQUEST['action'] == "register") {
    //rejestracja nowego użytkownika
        $db = new mysqli("localhost", "root", "", "burgerownia");
        $email = $_REQUEST['email'];
        //wyczyść email
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    
        $password = $_REQUEST['password'];
        $passwordRepeat = $_REQUEST['passwordRepeat'];
    
        if($password == $passwordRepeat) {
            //hasła są identyczne - kontynuuj
            $q = $db->prepare("INSERT INTO users VALUES (NULL, ?, ?)");
            $passwordHash = password_hash($password, PASSWORD_ARGON2I);
            $q->bind_param("ss", $email, $passwordHash);
            $result = $q->execute();
            if($result) {
                echo "Konto utworzono poprawnie"; 
				header('Location: zaloguj.php');
            } else {
                echo "Coś poszło nie tak!";
            }
        } else {
    
            echo "Hasła nie są zgodne - spróbuj ponownie!";
        }
    }

?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Royal Burgers</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />		
		<link href="style.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<header>
				<div class="logo"><img src="images/logo.png"></div>
				<div class="menu">
					<a href="index.php">Home</a>
					<a href="menu.php">Menu</a>
					<a href="cart.php">Koszyk</a>
					<a href="zaloguj.php" class="active">Zaloguj</a>
				</div>
			</header>
			<div class="content">
				<div class="text">
					<h1>Witamy w <b>Royal Burgers</b>!</h1>
					<h2>Zarejestruj się!<br>
                    <form action="rejestracja.php" method="post">
					<label for="emailInput">Email:</label>
    				<input type="email" name="email" id="emailInput" required><br>
					<label for="passwordInput">Hasło:</label>
    				<input type="password" name="password" id="passwordInput" required><br>
                    <label for="passwordRepeatInput">Hasło ponownie:</label>
                    <input type="password" name="passwordRepeat" id="passwordRepeatInput" required><br>
    				<input type="hidden" name="action" value="register">
					<input type="submit" value="Zarejestruj się!" style="font-size: 20px"></h2>
                    </form>
				</div>
				<div class="image"><img src="images/burger.png"></div>
			</div>
			<footer>
				<div>ul. Przemysłowa 20,<br />61-872 Poznań</div>
				<div><b>Godziny otwarcia:</b><br />Pn-Nd 11:00-23:00</div>
				<div><b>Telefon:</b> 800 758 749<br /><b>E-mail:</b> rburgers@gmail.com
			</footer>
		</div>
	</body>
</html>