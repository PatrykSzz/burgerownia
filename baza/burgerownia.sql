-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 08 Sty 2023, 13:11
-- Wersja serwera: 10.4.24-MariaDB
-- Wersja PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `burgerownia`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkty`
--

CREATE TABLE `produkty` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `description` text NOT NULL,
  `typ` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `produkty`
--

INSERT INTO `produkty` (`id`, `name`, `price`, `description`, `typ`) VALUES
(1, 'Royal Cesar', '33.00', 'Wołowina 150g, dip z sera feta, rukola, ser halloumi grillowany, ogórek zielony, pomidor, tzatziki, czerwona cebula', 'burger'),
(2, 'Royal Aloha', '31.00', 'Wołowina 150g, grillowany ananas, burak rukola, kiełki, karmelizowana cebula czerwona, ser cheddar, sos szefa, sos ostry', 'burger'),
(3, 'Royal Napoli', '29.00', 'Wołowina 150g, rukola, oliwki, suszone pomidory, szynka parmeńska, parmezan oraz sos tatarski', 'burger'),
(4, 'Royal King', '30.00', 'Wołowina 150g, ser cheddar, sałata, grillowane jajko, cebula, bekon i sos tatarski', 'burger'),
(5, 'Royal Stinky', '28.00', 'Wołowina 150g, ser camembert, ser limburski, rukola, karmelizowana czerwona cebula, pomidor, ogórek konserwowy, oliwa truflowa', 'burger'),
(6, 'Royal Joker', '35.00', 'Wołowina 150g, ser cheddar topiony, sałata, surówka szefa, krążki cebulowe, czerwona cebula, sos jogurtowo-pomidorowy', 'burger'),
(7, 'Pepsi 0,5l', '8.00', '', 'napoj'),
(8, 'Mirinda 0,5l', '8.00', '', 'napoj'),
(9, '7up 0,5l', '8.00', '', 'napoj'),
(10, 'Lipton Ice Tea 0,5l', '8.00', '', 'napoj'),
(11, 'Fritz Cola 0,5l', '8.00', '', 'napoj'),
(12, 'Coca Cola 0,5l', '8.00', '', 'napoj'),
(13, 'BBQ', '3.00', '', 'sos'),
(14, 'BBQ Musztardowy', '3.00', '', 'sos'),
(15, 'Sos serowy', '3.00', '', 'sos'),
(16, 'Sos jogurtowy', '3.00', '', 'sos'),
(17, 'Ketchup', '3.00', '', 'sos'),
(18, 'Sos mango-mayo', '3.00', '', 'sos'),
(19, 'Ostry', '3.00', '', 'sos'),
(20, 'Royal Zucchini', '30.00', 'Burger z cukini 150g, sałata, pomidor, kiełki, tzatziki, pomidor, cebula czerwona, rukola, sos szefa', 'vegeburger'),
(21, 'Royal Vege', '28.00', 'Burger z cukini 150g, sałata, pomidor, kiełki, tzatziki, pomidor, cebula czerwona, rukola, sos szefa', 'vegeburger'),
(22, 'Royal Onion', '33.00', 'Burger jaglano-warzywny 150g, kurczak vege 150g, cheddar, sałata, cebularz, cebulz czerwona, ogórek kiszony, sos szefa, ostry', 'vegeburger'),
(23, 'Royal Paprika', '28.00', 'Burger jaglano-warzywny 150g, Wołowina wege 150g, ser cheddar, sałata, grillowana papryka łagodna lub jalapeño, cebula, bekon oraz sos tatarski i ketchup', 'vegeburger');

--
-- Indeksy dla tabeli `produkty`
--
ALTER TABLE `produkty`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla tabeli `produkty`
--
ALTER TABLE `produkty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
