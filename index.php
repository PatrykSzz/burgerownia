<?php

	session_start();
	
	if ((isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']==true))
	{
		header('Location: index_zalogowany.php');
		exit();
	}

?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Royal Burgers</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />		
		<link href="style.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<header>
				<div class="logo"><img src="images/logo.png"></div>
				<div class="menu">
					<a href="index.php" class="active">Home</a>
					<a href="menu.php">Menu</a>
					<a href="cart.php">Koszyk</a>
					<a href="zaloguj.php">Zaloguj</a>
				</div>
			</header>
			<div class="content">
				<div class="text">
					<h1>Witamy w <b>Royal Burgers</b>!</h1>
					<h2>Rzemieślnicza burgerownia z Poznania</h2>
					<p>Jesteśmy "prawdziwą" burgerownią, która od niedawna pojawiła się w Poznaniu. Zaczęliśmy naszą przygodę w roku 2020. Proponujemy Wam burgery ze 100% mięsa wołowego specjalnie selekcjonowanego i przygotowywanego dla nas. Bułki pieczone są w jednej z najstarszych poznańskich piekarni, w sposób naturalny i według naszej receptury. Korzystamy tylko z najlepszych składników, świeżych warzyw, sosów które sami dla was przygotowujemy, oraz wkładamy nasze całe serce żeby każdy burger którego od nas dostaniecie, był nie tylko pyszny, ale i wyjątkowy.</p>
				</div>
				<div class="image"><img src="images/burger.png"></div>
			</div>
			<footer>
				<div>ul. Przemysłowa 20,<br />61-872 Poznań</div>
				<div><b>Godziny otwarcia:</b><br />Pn-Nd 11:00-23:00</div>
				<div><b>Telefon:</b> 800 758 749<br /><b>E-mail:</b> rburgers@gmail.com
			</footer>
		</div>
	</body>
</html>