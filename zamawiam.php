<?php
session_start();
if (!isset($_SESSION['zalogowany']))
	{
		header('Location: zaloguj.php');
		exit();
	}
	// pobranie informacji o produktach z koszyka z sesji
	$cart_items = $_SESSION['cart'];
	// połączenie z bazą danych
	$conn = new mysqli('localhost', 'root', '', 'burgerownia');
	
	// dla każdego produktu z koszyka

	foreach ($cart_items as $key=>$value) {
		// Dodanie danych do bazy danych
		$query = "INSERT INTO cart_items (name, price) VALUES ('$key', '$value')";
		mysqli_query($conn, $query);
	}
	// usunięcie produktów z koszyka z sesji
	unset($_SESSION['cart']);
	
	// zamknięcie połączenia z bazą danych
	$conn->close();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Royal Burgers</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />		
		<link href="style.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<header>
				<div class="logo"><img src="images/logo.png"></div>
				<div class="menu">
                    <a href="index_zalogowany.php">Home</a>
					<a href="menu_zalogowany.php">Menu</a>
					<a href="cart_zalogowany.php" class="active">Koszyk</a>
					<a href="wyloguj.php">Wyloguj <?php echo $_SESSION['email']; ?></a> 
				</div>
			</header>
			<div class="content">
				<div class="text">
					<h1>Dziękujemy za złożenie zamówienia w <b>Royal Burgers</b>!</h1>
					<h1>Odpocznij, a my przygotujemy dla Ciebie Twojego ulubionego burgera :)</h1>
					<h1>Oczekiwany czas realizacji zamówienia: 40-50 min </h1>
                    <h1>Życzymy <b>smacznego</b>!</h1>
				</div>
				<div class="image"><img src="images/burger.png"></div>
			</div>
			<footer>
				<div>ul. Przemysłowa 20,<br />61-872 Poznań</div>
				<div><b>Godziny otwarcia:</b><br />Pn-Nd 11:00-23:00</div>
				<div><b>Telefon:</b> 800 758 749<br /><b>E-mail:</b> rburgers@gmail.com
			</footer>
		</div>
	</body>
</html>