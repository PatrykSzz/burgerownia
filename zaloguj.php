<?php
session_start();
if (isset($_SESSION['zalogowany']))
	{
		echo "JESTEŚ JUŻ ZALOGOWANY!";
		exit();
	}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == "login") {
    $email = $_REQUEST['email'];
    $password = $_REQUEST['password'];

    $email = filter_var($email, FILTER_SANITIZE_EMAIL);

    //obiektowo
    $db = new mysqli("localhost", "root", "", "burgerownia");



    //prepared statements
    $q = $db->prepare("SELECT * FROM users WHERE email = ? LIMIT 1");
    //podstaw wartości
    $q->bind_param("s", $email);
    //wykonaj
    $q->execute();
    $result = $q->get_result();

    $userRow = $result->fetch_assoc();
    if ($userRow == null) {
        //konto nie istnieje
        echo "Błędny login lub hasło <br>";
    } else {
        //konto istnieje
        if (password_verify($password, $userRow['password'])) {
            //hasło poprawne
			if($userRow['email']=='admin@admin')
			{
				$_SESSION['admin'] = true;
				header('Location:index_admin.php');
			}
			else
			{
				$_SESSION['zalogowany'] = true;
				header('Location:index_zalogowany.php');
			}
			$_SESSION['id_users'] = $userRow['id_users'];
			$_SESSION['email'] = $userRow['email'];
        } else {
            //hasło niepoprawne
            echo "Błędny login lub hasło <br>";
        }
    }
}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Royal Burgers</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />		
		<link href="style.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<header>
				<div class="logo"><img src="images/logo.png"></div>
				<div class="menu">
					<a href="index.php">Home</a>
					<a href="menu.php">Menu</a>
					<a href="cart.php">Koszyk</a>
					<a href="zaloguj.php" class="active">Zaloguj</a>
				</div>
			</header>
			<div class="content">
				<div class="text">
					<h1>Witamy w <b>Royal Burgers</b>!</h1>
					<h2>Zaloguj się!<br>
					<form action="zaloguj.php" method="post">
					<label for="emailInput">Email:</label>
    				<input type="email" name="email" id="emailInput" required><br>
					<label for="passwordInput">Hasło:</label>
    				<input type="password" name="password" id="passwordInput" required><br>
					<input type="hidden" name="action" value="login">
					<input type="submit" value="Zaloguj się!" style="font-size: 30px"><br>
					</form>
					<a href="rejestracja.php">Nie masz konta? Zarejestruj się!</a></h2>
				</div>
				<div class="image"><img src="images/burger.png"></div>
			</div>
			<footer>
				<div>ul. Przemysłowa 20,<br />61-872 Poznań</div>
				<div><b>Godziny otwarcia:</b><br />Pn-Nd 11:00-23:00</div>
				<div><b>Telefon:</b> 800 758 749<br /><b>E-mail:</b> rburgers@gmail.com
			</footer>
		</div>
	</body>
</html>